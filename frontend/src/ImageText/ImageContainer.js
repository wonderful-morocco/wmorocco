import React from 'react';
import './ImageContainer.css'
const ImageContainer = () => {
    return (
        <>
            <div className="imageContainer">
            <img
                src="./img/image20.png"
                alt=""
                className="image"
            />
            <div className="text-overlay">
                <h1 className="text">Plan your trip with travel </h1>
                <h1 className="text2" id=''>expert</h1>
                <h4>Our professional advisors can craft your perfect itinerary</h4>
            </div>
        </div></>
    );
};
        


export default ImageContainer;
