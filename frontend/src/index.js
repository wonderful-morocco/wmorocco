// import React from 'react';
// import ReactDOM from 'react-dom/client';
// import HomePage from './HomePage';
// import PostPage from './PostPage/PostPage';
// import App from './App';
// // import './index.css';
// import reportWebVitals from './reportWebVitals';

// // import ProductList from './ProductList';
// // import 'bootstrap/dist/css/bootstrap.css';
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <>
//     <App />
    
   
//   </>
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; // Assuming you have some global styles
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

