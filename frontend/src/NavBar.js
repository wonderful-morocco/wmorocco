import React from 'react';
import { Link } from 'react-router-dom';

function NavBar() {
  return (
    <nav>
      <a href="/">WMorocco</a>
      <ul className="list">
        <li><Link to="/">Home</Link></li>
        <li><Link to="/hebergements">Hébergements</Link></li>
        <li><Link to="/">Flights</Link></li>
        <li><Link to="/">Packages</Link></li>
        <li><Link to="/">Sign up</Link></li>
      </ul>
    </nav>
  );
}

export default NavBar;
