import React, { useState } from 'react';
import './PostPage.css'; // Import PostPage CSS
import './FilterOptions.css'; // Import the FilterOptions CSS
import PostPageData from './PostPageData.json'; // Import the SearchBar component

const PostPage = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [locationFilter, setLocationFilter] = useState('');
  const [ratingFilter, setRatingFilter] = useState('');
  const [minBudget, setMinBudget] = useState(0);
  const [maxBudget, setMaxBudget] = useState(20000);

  const handleLike = (hotelName) => {
    alert(`You liked ${hotelName}`);
  };

  // Function to convert USD to Moroccan Dirhams (MAD)
  const convertToMAD = (usdPrice) => {
    const conversionRate = 9.65;
    return (usdPrice * conversionRate).toFixed(2);
  };

  // Function to get unique values for filter options
  const getUniqueValues = (key) => {
    const uniqueValues = new Set();
    PostPageData.forEach((hotel) => {
      uniqueValues.add(hotel[key]);
    });
    return Array.from(uniqueValues);
  };

  // Filter hotels based on search term, location, rating, and budget
  const filteredHotels = PostPageData.filter((hotel) => {
    const matchesSearchTerm = hotel.name.toLowerCase().includes(searchTerm.toLowerCase());
    const matchesLocation = locationFilter === '' || hotel.location === locationFilter;
    const matchesRating = ratingFilter === '' || hotel.rating === parseFloat(ratingFilter);
    const hotelPriceMAD = parseFloat(convertToMAD(hotel.price));
    const matchesBudget = hotelPriceMAD >= minBudget && hotelPriceMAD <= maxBudget;
    return matchesSearchTerm && matchesLocation && matchesRating && matchesBudget;
  });

  return (
    <>
      {filteredHotels.length === 0 && (
        <p className="no-hotels-message">No hotels found with the selected criteria.</p>
      )}
      
      <div className="filter-bar">
        <input
          type="text"
          placeholder="Search..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <label htmlFor="locationFilter">Location:</label>
        <select id="locationFilter" value={locationFilter} onChange={(e) => setLocationFilter(e.target.value)}>
          <option value="">All Locations</option>
          {getUniqueValues('location').map((location) => (
            <option key={location} value={location}>{location}</option>
          ))}
        </select>

        <label htmlFor="ratingFilter">Rating:</label>
        <select id="ratingFilter" value={ratingFilter} onChange={(e) => setRatingFilter(e.target.value)}>
          <option value="">All Ratings</option>
          {[1, 2, 3, 4, 5].map((rating) => (
            <option key={rating} value={rating}>{rating} Stars</option>
          ))}
        </select>

        <label htmlFor="budgetRange">Budget (MAD):</label>
        <input
          type="range"
          id="minBudget"
          min="0"
          max="10000"
          step="100"
          value={minBudget}
          onChange={(e) => setMinBudget(e.target.value)}
        />
        <input
          type="range"
          id="maxBudget"
          min="0"
          max="10000"
          step="100"
          value={maxBudget}
          onChange={(e) => setMaxBudget(e.target.value)}
        />
        <span>{minBudget} - {maxBudget} MAD</span>
      </div>

      <div className="card-list">
        {filteredHotels.length > 0 ? (
          filteredHotels.map((hotel, index) => (
            <article className="card" key={index}>
            <div className="card-image">
              <img src={hotel.image} alt={hotel.name} />
              <button className="like-button" onClick={() => handleLike(hotel.name)}>
                <i className="fas fa-heart"></i>
              </button>
            </div>
            <div className="card-content">
              <div className="card-header">
                <h2>{hotel.name}</h2>
                <p>{hotel.location}</p>
                <div className="card-rating">
                  <span>Rating: {hotel.rating}</span>
                </div>
              </div>
              <div className="card-body">
                <ul>
                  {hotel.amenities.map((amenity, i) => (
                    <li key={i}>{amenity}</li>
                  ))}
                </ul>
              </div>
            </div>
            <div className="card-footer">
              <div className="card-price">
                <p>{convertToMAD(hotel.price)} MAD per night</p>
              </div>
            </div>
          </article>
          ))
        ) : null}
      </div>
    </>
  );
};

export default PostPage;
