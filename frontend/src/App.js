import React from 'react';
import { Route, Routes } from 'react-router-dom';
import HomePage from './HomePage';
import PostPage from './PostPage/PostPage';

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/hebergements" element={<PostPage />} />
    </Routes>
  );
}

export default App;
