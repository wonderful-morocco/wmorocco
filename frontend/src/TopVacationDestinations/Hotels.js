import React, { useRef } from 'react';
import hotelsData from './hotelsData.json';
import './Hotels.css';

const Hotels = () => {
  const cardContainerRef = useRef(null);

  const scrollLeft = () => {
    if (cardContainerRef.current) {
      cardContainerRef.current.scrollBy({ left: -1050, behavior: 'smooth' }); // scroll left by 3 cards (3 * 350px)
    }
  };

  const scrollRight = () => {
    if (cardContainerRef.current) {
      cardContainerRef.current.scrollBy({ left: 1050, behavior: 'smooth' }); // scroll right by 3 cards (3 * 350px)
    }
  };

  return (
   <>
   <h3>Top Vacation Destinations</h3>
    <div className="center">
      <button className="scroll-button left" onClick={scrollLeft}>{"<"}</button>
      <div className="card-container" ref={cardContainerRef}>
        {hotelsData.map((hotel, index) => (
          <div className="article-card" key={index}>
            <div className="content">
              <p className="title">{hotel.name}</p>
            </div>
            <img src={hotel.image} alt={hotel.name} />
          </div>
        ))}
      </div>
      <button className="scroll-button right" onClick={scrollRight}>{">"}</button>
    </div>
    </> 
  );
};

export default Hotels;
