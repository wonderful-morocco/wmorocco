import React, { useRef } from 'react';
import CitiesData from '../FilterCities/CitiesData.json';
import styles from './HotelsFilter.module.css';

const HotelsFilterByLocation = () => {
  const cardContainerRef = useRef(null);


  return (
    <div className={styles.container}>
     
      <div className={styles.cardContainer} ref={cardContainerRef}>
        {CitiesData.map((city, index) => (
          <article className={styles.card} key={index}>
            <img
              className={styles.cardBackground}
              src={city.image}
              alt={city.location}
              onError={(e) => { e.target.src = '/path/to/fallback-image.png'; }} // Add the correct path to fallback image
            />
            <div className={`${styles.cardContent} ${styles.flow}`}>
              <div className={`${styles.cardContentContainer} ${styles.flow}`}>
                <h2 className={styles.cardTitle}>{city.location}</h2>
                <p className={styles.cardDescription}>
                  Discover amazing hotels and experiences in {city.location}.
                </p>
              </div>
            </div>
          </article>
        ))}
      </div>
      
    </div>
  );
};

export default HotelsFilterByLocation;
