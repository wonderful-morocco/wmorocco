import React from 'react';

const Footer = () => {
  return (
    <>

      <footer className="footer">
        <div className="footer__addr">
          <h1 className="footer__logo">WONDERFUL <br/>  morocco</h1>
          
          <h2>Contact</h2>
          
          <address>
            5534 Somewhere In. The World 22193-10212<br />
            
            <a className="footer__btn" href="mailto:example@gmail.com">Email Us</a>
          </address>
        </div>
        
        <ul className="footer__nav">
          <li className="nav__item">
            <h2 className="nav__title">Support</h2>

            <ul className="nav__ul">
              <li>
                <a href="/">Help Center</a>
              </li>

              <li>
                <a href="/">AirCover</a>
              </li>
              
              <li>
                <a href="/">Disability support</a>
              </li>
            </ul>
          </li>
          
          <li className="nav__item nav__item--extra">
            <h2 className="nav__title">Hosting</h2>
            
            <ul className="nav__ul nav__ul--extra">
              <li>
                <a href="/">WM your home</a>
              </li>
              
              <li>
                <a href="/">Aircover for Hosts</a>
              </li>
              
              <li>
                <a href="/">Hosting ressources</a>
              </li>
              
              <li>
                <a href="/">Community forum</a>
              </li>
              
              <li>
                <a href="/">Hosting responsibly</a>
              </li>
              
        
            </ul>
          </li>
          
          <li className="nav__item">
            <h2 className="nav__title">Wonderful Morocco</h2>
            
            <ul className="nav__ul">
              <li>
                <a href="/">Newsroom</a>
              </li>
              
              <li>
                <a href="/">New features</a>
              </li>
              
              <li>
                <a href="/">About</a>
              </li>
            </ul>
          </li>
        </ul>
        
        <div className="legal">
          <p>&copy; 2024 Something. All rights reserved.</p>
          
          <div className="legal__links">
            <span>Made with <span className="heart">♥</span> remotely from Anywhere</span>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
