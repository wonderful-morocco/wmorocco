import React, { useRef, useState } from 'react';
import offersData from './offersData.json';
import './Offers.css';

const getRandomOffers = (data, count) => {
  const shuffled = [...data].sort(() => 0.5 - Math.random());
  return shuffled.slice(0, count);
};

const Offers = () => {
  const cardContainerRef = useRef(null);
  const randomOffers = getRandomOffers(offersData, 8); // Show 8 random offers
  const [expanded, setExpanded] = useState(Array(randomOffers.length).fill(false));

  const toggleReadMore = (index) => {
    setExpanded((prev) => {
      const newState = [...prev];
      newState[index] = !newState[index];
      return newState;
    });
  };

  const scrollContainer = (direction) => {
    if (cardContainerRef.current) {
      const scrollAmount = 300;
      cardContainerRef.current.scrollBy({
        left: direction === 'left' ? -scrollAmount : scrollAmount,
        behavior: 'smooth'
      });
    }
  };

  return (
    <>
    <h3>Offers</h3>
    <div className="center">
      <button className="scroll-button left" onClick={() => scrollContainer('left')}>{'<'}</button>
      <div className="containerOffers" ref={cardContainerRef}>
        {randomOffers.map((offer, index) => (
          <div className="card" key={index}>
            <img src={offer.image} alt={offer.name} onError={(e) => { e.target.onerror = null; e.target.src = '/images/default.jpg'; }} />
            <div className="card-body">
              <div className="card-text">
                <h3>{offer.name}</h3>
                <p>
                  {expanded[index] ? offer.description : `${offer.description.substring(0, 100)}...`}
                  {offer.description.length > 100 && (
                    <span className="read-more" onClick={() => toggleReadMore(index)}>
                      {expanded[index] ? " Read Less" : " Read More"}
                    </span>
                  )}
                </p>
                <p className="price">{offer.price}</p>
                <p className="location">{offer.location}</p>
              </div>
              <button className="book-button">Book Now</button>
            </div>
          </div>
        ))}
      </div>
      <button className="scroll-button right" onClick={() => scrollContainer('right')}>{'>'}</button>
    </div>
    </>
  );
};

export default Offers;
