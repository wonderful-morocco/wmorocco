import React from 'react';
import './HomePage.css'; // Assuming you have a CSS file for styling
import NavBar from './NavBar';
import Footer from './footer';
import Offers from './Offers/Offers';
import Hotels from './TopVacationDestinations/Hotels';
import ImageContainer from './ImageText/ImageContainer';
import HotelsFilterByLocation from './FilterCities/HotelsFilterByLocation';
const HomePage = () => {
  return (
    <>
    
        
      <div id="booking" className="section">
      
        <div className="section-center">
          <div><NavBar /></div>
          <div className="container">
           
            <div className="row">
               
              <div className="col-md-7 col-md-push-5">
               
                <div className="booking-cta">
                
                  <h1>The whole world awaits.</h1>
                  <p>
                  Trouvez votre prochain séjour
Recherchez des offres sur des hôtels, des hébergements indépendants et plus encore
                  </p>
                </div>
              </div>
              <div className="col-md-4 col-md-pull-7">
                <div className="booking-form">
                  <form>
                    <div className="form-group">
                      <span className="form-label">Search destinations, hotels</span>
                      <input className="form-control" type="text" placeholder="Enter a destination or hotel name" />
                    </div>
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="form-group">
                          <span className="form-label">Check In</span>
                          <input className="form-control" type="date" required />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <span className="form-label">Check Out</span>
                          <input className="form-control" type="date" required />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-4">
                        <div className="form-group">
                          <span className="form-label">Rooms</span>
                          <select className="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <div className="form-group">
                          <span className="form-label">Adults</span>
                          <select className="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <div className="form-group">
                          <span className="form-label">Children</span>
                          <select className="form-control">
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="form-btn">
                      <button className="submit-btn">Check availability</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div className='posts'> 
      <Hotels />
      <Offers/>
      <ImageContainer/>
      <HotelsFilterByLocation/>
      </div>
      
      <Footer/>
      </>
   
  );
};

export default HomePage;
