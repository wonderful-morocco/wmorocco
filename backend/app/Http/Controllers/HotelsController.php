<?php

namespace App\Http\Controllers;


use App\Models\hotels;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $hotels = hotels::all();
        return response()->json($hotels);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $request->validate([
            'username' => 'sometimes|required|string|max:255',
            'password' => 'required|string|min:8|confirmed',
            'email' => 'required|string|email|max:255|unique:clients',
            'owner_id' => $user->id,
            'name' => $request->name,
            'address' => $request->address,
            'city'=> $request->city,
            'phone' => $request->phone,
            'website' => $request->website,
            'photos' => $request->photo,
            'description' => $request->description,
            'price' => $request->price,
        ]);

        $user= hotels::create($request->all());
        return response()->json($user, 201);

        $hotels = hotels::create([
            'owner_id' => $user->id,
            'name' => $request->name,
            'address' => $request->address,
            'city'=> $request->city,
            'phone' => $request->phone,
            'website' => $request->website,
            'photos' => $request->photo,
            'description' => $request->description,
            'price' => $request->price,
        ]);

        return response()->json($hotels, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $hotels = hotels::find($id);
        return response()->json($hotels);
    }
    
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id) {
        $user = JWTAuth::parseToken()->authenticate();
        $hotels = hotels::find($id);

        if ($hotels->owner_id !== $user->id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $hotels->update($request->all());
        return response()->json($hotels);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {
        $user = JWTAuth::parseToken()->authenticate();
        $hotels = hotels::find($id);

        if ($hotels->owner_id !== $user->id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $hotels->delete();
        return response()->json(['message' => 'Hotel deleted successfully']);
    }
}
