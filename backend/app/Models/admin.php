<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    use HasFactory;
    protected $primaryKey = 'owner_id';

    protected $fillable = [
        'name',
        'email',
        'password',
        'hotel_id',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function hotel() {
        return $this->hasMany(Hotel::class);
    }
}
