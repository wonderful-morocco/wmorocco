<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class review extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'hotel_id',
        'review',
        'rating',
    ];

    
    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }

}
