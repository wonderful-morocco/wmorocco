<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class hotels extends Model
{
    use HasFactory;

    protected $primaryKey = 'hotel_id';

    protected $fillable = [
        'owner_id',
        'name',
        'address',
        'city',
        'phone',
        'website',
        'photos', 
        'description',
        'price',
    ];
    
    public function admin() {
        return $this->belongsTo(Admin::class);
    }

    public function user(){
        return $this->hasMany(User::class);
    }

}
