<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\ClientController;

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/user', [AuthController::class, 'user'])->middleware('auth:sanctum');
Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::get('protected-route', 'ProtectedController@index')->middleware('auth:api');


Route::middleware(['jwt.verify'])->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('profile', [AuthController::class, 'profile']);
    
    
    Route::get('hotels', [HotelController::class, 'index']);
    Route::get('hotels/{hotel_id}', [HotelController::class, 'show']);
    Route::post('hotels', [HotelController::class, 'store']);
    Route::put('hotels/{hotel_id}', [HotelController::class, 'update']);
    Route::delete('hotels/{hotel_id}', [HotelController::class, 'destroy']);

    Route::get('reviews', [ReviewController::class, 'index']);
    Route::get('reviews/{review_id}', [ReviewController::class, 'show']);
    Route::post('reviews', [ReviewController::class, 'store']);
    Route::put('reviews/{review_id}', [ReviewController::class, 'update']);
    Route::delete('reviews/{review_id}', [ReviewController::class, 'destroy']);

    Route::get('clients', [ClientController::class, 'index']);
    Route::get('clients/{client_id}', [ClientController::class, 'show']);
    Route::post('clients', [ClientController::class, 'store']);
    Route::put('clients/{client_id}', [ClientController::class, 'update']);
    Route::delete('clients/{client_id}', [ClientController::class, 'destroy']);
    
});